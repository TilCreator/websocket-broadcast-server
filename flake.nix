{
  description = "websocket relay that allows easy client to client communication over websockets";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };

    pre-commit = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    crane,
    flake-utils,
    advisory-db,
    pre-commit,
    ...
  }:
    (flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
      };

      inherit (pkgs) lib;

      craneLib = crane.mkLib nixpkgs.legacyPackages.${system};
      src = craneLib.cleanCargoSource ./.;

      buildInputs = with pkgs;
        [
          openssl
          pkg-config
        ]
        ++ lib.optionals stdenv.isDarwin
        (with darwin; [
          apple_sdk.frameworks.CoreFoundation
          apple_sdk.frameworks.CoreServices
          apple_sdk.frameworks.SystemConfiguration
          libiconv
        ]);

      # Build *just* the cargo dependencies, so we can reuse
      # all of that work (e.g. via cachix) when running in CI
      cargoArtifacts = craneLib.buildDepsOnly {
        inherit src buildInputs;
      };

      # Build the actual crate itself, reusing the dependency
      # artifacts from above.
      my-crate = craneLib.buildPackage {
        inherit cargoArtifacts src buildInputs;
      };
    in {
      checks =
        {
          # Build the crate as part of `nix flake check` for convenience
          inherit my-crate;

          # Run clippy (and deny all warnings) on the crate source,
          # again, resuing the dependency artifacts from above.
          #
          # Note that this is done as a separate derivation so that
          # we can block the CI if there are issues here, but not
          # prevent downstream consumers from building our crate by itself.
          my-crate-clippy = craneLib.cargoClippy {
            inherit cargoArtifacts src;
            cargoClippyExtraArgs = "--all-targets -- --deny warnings";
          };

          my-crate-doc = craneLib.cargoDoc {
            inherit cargoArtifacts src;
          };

          # Check formatting
          my-crate-fmt = craneLib.cargoFmt {
            inherit src;
          };

          # Audit dependencies
          my-crate-audit = craneLib.cargoAudit {
            inherit src advisory-db;
          };

          # Run tests with cargo-nextest
          # Consider setting `doCheck = false` on `my-crate` if you do not want
          # the tests to run twice
          my-crate-nextest = craneLib.cargoNextest {
            inherit cargoArtifacts src;
            partitions = 1;
            partitionType = "count";
          };
        }
        // (lib.optionalAttrs (system == "x86_64-linux") {
          # NB: cargo-tarpaulin only supports x86_64 systems
          # Check code coverage (note: this will not upload coverage anywhere)
          my-crate-coverage = craneLib.cargoTarpaulin {
            inherit cargoArtifacts src;
          };
        })
        // {
          pre-commit-check = pre-commit.lib.${system}.run {
            src = ./.;
            hooks = {
              editorconfig-checker.enable = true;
              alejandra.enable = true;
              deadnix.enable = true;
              flake-checker.enable = true;
              statix.enable = true;
            };
          };
        }
        // {
          nixosTest = nixpkgs.lib.nixos.runTest {
            name = "websocket-broadcast-serverDefault";
            hostPkgs = nixpkgs.legacyPackages.${system};
            imports = [
              {
                nodes = {
                  node1 = {pkgs, ...}: {
                    imports = [
                      self.nixosModules.default
                      {
                        environment.systemPackages = [pkgs.websocat];
                        services.websocket-broadcast-server.enable = true;
                      }
                    ];
                  };
                };
                testScript = ''
                  start_all()

                  node1.wait_for_unit("websocket-broadcast-server")
                  node1.succeed("echo wah | websocat ws://localhost:9001/test")
                '';
              }
            ];
          };
        };

      packages = {
        default = my-crate;
        websocket-broadcast-server = my-crate;
      };

      apps.default = flake-utils.lib.mkApp {
        drv = my-crate;
      };

      devShells.default = pkgs.mkShell {
        inherit (self.checks.${system}.pre-commit-check) shellHook;

        inputsFrom = builtins.attrValues self.checks;

        # Extra inputs can be added here
        nativeBuildInputs = with pkgs; [
          cargo
          clippy
          rustfmt
          rustc
        ];
      };
    }))
    // {
      nixosModules.default = {
        config,
        lib,
        pkgs,
        ...
      }: let
        cfg = config.services.websocket-broadcast-server;
      in {
        options.services.websocket-broadcast-server = {
          package = lib.mkPackageOption self.packages.${pkgs.system} "websocket-broadcast-server" {};
          enable = lib.mkEnableOption "websocket-broadcast-server";
          user = lib.mkOption {
            default = "websocket-broadcast-server";
            type = lib.types.str;
            description = ''
              User account under which websocket-broadcast-server runs.

              ::: {.note}
              If left as the default value this user will automatically be created
              on system activation, otherwise you are responsible for
              ensuring the user exists.
              :::
            '';
          };
          group = lib.mkOption {
            default = "websocket-broadcast-server";
            type = lib.types.str;
            description = ''
              Group account under which websocket-broadcast-server runs.

              ::: {.note}
              If left as the default value this user will automatically be created
              on system activation, otherwise you are responsible for
              ensuring the user exists.
              :::
            '';
          };
          verbose = lib.options.mkOption {
            type = lib.types.bool;
            description = "Enables verbose output";
            default = false;
            example = true;
          };
          log-msgs = lib.options.mkOption {
            type = lib.types.bool;
            description = "Enables logging of send broadcasts and connected clients";
            default = false;
            example = true;
          };
          listen-address = lib.options.mkOption {
            type = lib.types.str;
            description = "Set where the server is listening";
            default = "[::]:9001";
            example = "[::]:9001";
          };
          delay = lib.options.mkOption {
            type = lib.types.float;
            description = "Delay to create an abitrary long ping (for testing)";
            default = 0.0;
            example = 0.5;
          };
        };

        config = lib.mkIf cfg.enable {
          systemd.services.websocket-broadcast-server = {
            after = ["network.target" "network-online.target"];
            requires = ["network-online.target"];
            wantedBy = ["multi-user.target"];
            description = "websocket-broadcast-server";
            confinement.enable = true;
            script = builtins.concatStringsSep " " (
              [
                "${cfg.package}/bin/websocket-broadcast-server"
                "--listen-address \"${cfg.listen-address}\""
                "--delay ${toString cfg.delay}"
              ]
              ++ (lib.lists.optional cfg.verbose "--verbose")
              ++ (lib.lists.optional cfg.log-msgs "--log-msgs")
            );
            serviceConfig = {
              User = cfg.user;
              Group = cfg.group;
              Restart = "on-failure";
              RestartPreventExitStatus = 1;
              RestartSec = "5s";

              NoNewPrivileges = true;
              PrivateDevices = true;
              ProtectHome = true;
            };
          };
          users = {
            users = lib.optionalAttrs (cfg.user == "websocket-broadcast-server") {
              websocket-broadcast-server = {
                inherit (cfg) group;
                isSystemUser = true;
              };
            };
            groups = lib.optionalAttrs (cfg.group == "websocket-broadcast-server") {
              ${cfg.group} = {};
            };
          };
        };
      };
    };
}
