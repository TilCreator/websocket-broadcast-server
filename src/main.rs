use clap::Parser;
use futures::{FutureExt, StreamExt};
use std::collections::HashMap;
use std::net::{SocketAddr, ToSocketAddrs};
use std::sync::Arc;
use tokio::{
    sync::{mpsc, RwLock},
    time::{sleep, Duration},
};
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::ws::{Message, WebSocket};
use warp::Filter;

// Data structures to keep track of rooms and clients in rooms
type Rooms = Arc<RwLock<HashMap<String, Clients>>>;
type Clients = RwLock<HashMap<SocketAddr, mpsc::UnboundedSender<Result<Message, warp::Error>>>>;

/// websocket-broadcast-server
#[derive(Parser, Debug, Clone)]
#[command(version, about, long_about = None)]
struct Args {
    /// Enables verbose output
    #[arg(short, long)]
    verbose: bool,
    /// Enables logging of send broadcasts and connected clients
    #[arg(short = 'm', long)]
    log_msgs: bool,
    /// Set where the server is listening
    #[arg(short, long, default_value = "localhost:9001")]
    listen_address: String,
    /// Delay to create an abitrary long ping (for testing)
    #[arg(short, long, default_value = "0")]
    delay: f64,
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    // Keep track of all rooms and clients in the rooms
    let rooms = Rooms::default();
    // Make rooms a Filter, so it can be accessed below
    let rooms = warp::any().map(move || rooms.clone());
    // Make logging a Filter, so it can be accessed below
    let logging = warp::any().map(move || args.log_msgs);
    let delay = warp::any().map(move || args.delay);

    // Allow WS connections on any path
    let routes = warp::path!(String) // Use path variable to determine room
        .and(warp::ws()) // Use the WS protocol, initiate the handshake
        .and(rooms) // Use rooms hashmap
        .and(warp::addr::remote()) // Use clients addr to keep track of client
        .and(logging) // Use logging state
        .and(delay)
        .map(
            |path: String,
             ws: warp::ws::Ws,
             rooms,
             addr: Option<SocketAddr>,
             logging: bool,
             delay: f64| {
                // Accept client on handshake success
                ws.on_upgrade(move |socket| {
                    client_connected(socket, rooms, path, addr.unwrap(), logging, delay)
                })
            },
        );

    // Serve until the end of time (or kill)
    warp::serve(routes)
        .run(
            args.listen_address
                .to_socket_addrs()
                .unwrap()
                .next()
                .unwrap(),
        )
        .await;
}

async fn client_connected(
    ws: WebSocket,
    rooms: Rooms,
    room: String,
    addr: SocketAddr,
    logging: bool,
    delay: f64,
) {
    if logging {
        println!("Client {}#{} connected", addr, room);
    }

    // Split the socket into a sender and receive of messages.
    let (client_ws_tx, mut client_ws_rx) = ws.split();

    // Use an unbounded channel to handle buffering and flushing of messages to the websocket
    let (tx, rx) = mpsc::unbounded_channel();
    let rx = UnboundedReceiverStream::new(rx);
    tokio::task::spawn(rx.forward(client_ws_tx).map(|result| {
        if let Err(e) = result {
            eprintln!("websocket error: {}", e);
        }
    }));

    // Create room if nesesary
    if !rooms.read().await.contains_key(&room) {
        rooms.write().await.insert(room.clone(), Clients::default());
    }
    // Add client to the list of connected clients in this room
    rooms
        .write()
        .await
        .get(&room)
        .unwrap()
        .write()
        .await
        .insert(addr, tx);

    // Broadcast to all clients in the room on message received from client
    while let Some(result) = client_ws_rx.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                eprintln!("websocket error(addr={},room={}): {}", addr, room, e);
                break;
            }
        };
        client_message(addr, msg, &rooms, &room, &logging, delay).await;
    }

    // Gracefully handle disconnect
    client_disconnected(addr, &rooms, &room, &logging).await;
}

async fn client_message(
    my_addr: SocketAddr,
    msg: Message,
    rooms: &Rooms,
    room: &str,
    logging: &bool,
    delay: f64,
) {
    // Skip any messages that aren't text or binary
    if !msg.is_text() && !msg.is_binary() {
        return;
    }

    if *logging {
        println!(
            "Client {}#{} send: {}",
            my_addr,
            room,
            {
                if msg.is_text() {
                    match msg.clone().to_str() {
                        Ok(s) => String::from(s),
                        Err(_) => String::from("Error decoding msg"),
                    }
                } else if msg.is_binary() {
                    String::from("<binary>")
                } else {
                    String::from("<unknown>")
                }
            }
            .replace('\n', "\\n")
        );
    }

    // Send message to all other clients in the room
    for (&addr, tx) in rooms.read().await.get(room).unwrap().read().await.iter() {
        if my_addr != addr {
            sleep(Duration::from_secs_f64(delay)).await;

            // This can fail, but no catch needed, because the disconnect handle will handle it (hopefully)
            if let Err(_disconnected) = tx.send(Ok(msg.clone())) {}
        }
    }
}

async fn client_disconnected(addr: SocketAddr, rooms: &Rooms, room: &str, logging: &bool) {
    if *logging {
        println!("Client {}#{} disconnected", addr, room);
    }

    // Remove client from client list in room
    rooms
        .write()
        .await
        .get(room)
        .unwrap()
        .write()
        .await
        .remove(&addr);
    // Remove the room if empty
    if rooms.read().await.get(room).unwrap().read().await.len() == 0 {
        rooms.write().await.remove(room);
    }
}
