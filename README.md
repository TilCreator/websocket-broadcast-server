# Websocket broadcast server
Simple websocket server that broadcasts msgs from clients per room.
## Concept
### participents
- multiple clients
  Connect to one room on the server and send and receive broadcast msgs.
- one server
  Blindly delivers broadcast msgs from clients to all other clients in one room.
### Schematic
```
               /----------------\
               |     server     |
               |                |
client_0 -------- room_0        |
               |  |             |
client_1 ---------/             |
               |                |
client_2 -------- room_1        |
               |  |             |
client_3 ---------/             |
               \----------------/
```
### Shamelessly stolen from
- https://github.com/seanmonstar/warp/blob/master/examples/websockets_chat.rs
